#!/usr/bin/env python
# coding: utf-8

# In[27]:


import platform
from datetime import datetime

import numpy as np
from numpy import random


# implementation 1, hand crafted for loop
def matrix_multiplication_1(matrix_a, matrix_b, dtype='double'):
    row_n_a, col_n_a = matrix_a.shape
    row_n_b, col_n_b = matrix_b.shape

    result = np.zeros((row_n_a, col_n_b), dtype=dtype)
    for row_i in range(row_n_a):
        row_a = matrix_a[row_i, :]
        for col_i in range(col_n_b):
            col_b = matrix_b[:, col_i]

            sum_value = 0
            for a, b in zip(row_a, col_b):
                sum_value += a * b

            result[row_i, col_i] = sum_value

    return result


# implementation 2, use numpy
def matrix_multiplication_2(matrix_a, matrix_b):
    return np.dot(matrix_a, matrix_b)


# # integer

# In[28]:


total_time = 0
n = 10

for i in range(n):
    a = random.randint(-10, 10, size=(400, 200))
    b = random.randint(-10, 10, size=(200, 400))

    start_t = datetime.now()
    result = matrix_multiplication_1(a, b, dtype='int')
    end_t = datetime.now()

    time = (end_t - start_t).total_seconds()
    total_time += time

average_time = total_time / n
print('Implementation_1, integer, average time {}'.format(average_time))

# In[29]:


total_time = 0
n = 100

for i in range(n):
    a = random.randint(-10, 10, size=(400, 200))
    b = random.randint(-10, 10, size=(200, 400))

    start_t = datetime.now()
    result = matrix_multiplication_2(a, b)
    end_t = datetime.now()

    time = (end_t - start_t).total_seconds()
    total_time += time

average_time = total_time / n
print('Implementation_2, integer, average time {}'.format(average_time))

# double

# In[30]:


total_time = 0

n = 10

for i in range(n):
    a = random.uniform(-10, 10, size=(400, 200)).astype('double')
    b = random.uniform(-10, 10, size=(200, 400)).astype('double')

    start_t = datetime.now()
    result = matrix_multiplication_1(a, b, dtype='double')
    end_t = datetime.now()

    time = (end_t - start_t).total_seconds()
    total_time += time

average_time = total_time / n

print('Implementation_1, double, average time {}'.format(average_time))

# In[31]:


total_time = 0

n = 100

for i in range(n):
    a = random.uniform(-10, 10, size=(400, 200)).astype('double')
    b = random.uniform(-10, 10, size=(200, 400)).astype('double')

    start_t = datetime.now()
    result = matrix_multiplication_2(a, b)
    end_t = datetime.now()

    time = (end_t - start_t).total_seconds()
    total_time += time

average_time = total_time / n

print('Implementation_1, double, average time {}'.format(average_time))

# In[32]:


print(
    (
        platform.platform(),
        platform.python_compiler()
    )
)
